<?php

use SilverStripe\View\Parsers\URLSegmentFilter;

// prohibit "and" replacements in url segments
$urlSegmentFilterConfig = URLSegmentFilter::config();
$replacements = $urlSegmentFilterConfig->get('default_replacements');
$replacements['/&amp;/u'] = '-';
$replacements['/&/u'] = '-';
$urlSegmentFilterConfig->set('default_replacements', $replacements);
