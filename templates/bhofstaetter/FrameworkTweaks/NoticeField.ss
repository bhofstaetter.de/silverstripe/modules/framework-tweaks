<% if not $Hidden %>
    <% if not $FieldGroupTitle %>
        <div class="message $Severity">$Content</div>
    <% else %>
        <div class="field form-group">
            <label class="form__field-label">$FieldGroupTitle</label>
            <div class="form__fieldgroup form__field-holder">
                <div class="message $Severity" style="margin-bottom: 0;">$Content</div>
            </div>
        </div>
    <% end_if %>
<% end_if %>
