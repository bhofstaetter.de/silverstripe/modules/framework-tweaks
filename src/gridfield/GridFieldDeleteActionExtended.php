<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridField_ActionMenuItem;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ValidationException;

class GridFieldDeleteActionExtended extends GridFieldDeleteAction {

    /**
     * Handle the actions and apply any changes to the GridField
     *
     * @param GridField $gridField
     * @param string $actionName
     * @param array $arguments
     * @param array $data Form data
     * @throws ValidationException
     */
    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if ($actionName == 'unlinkrelation') {
            /** @var DataObject $item */
            $item = $gridField->getList()->byID($arguments['RecordID']);

            if (!$item) {
                return;
            }

            if (!$item->canUnlink(null, $gridField)) {
                throw new ValidationException(
                    _t(GridFieldDeleteAction::class . '.EditPermissionsFailure', "No permission to unlink record")
                );
            }

            $gridField->getList()->remove($item);
        }

        parent::handleAction($gridField, $actionName, $arguments, $data);
    }

    /**
     *
     * @param GridField $gridField
     * @param DataObject $record
     * @param string $columnName
     * @return string|null the HTML for the column
     */
    public function getColumnContent($gridField, $record, $columnName) {
        $field = parent::getColumnContent($gridField, $record, $columnName);

        if ($this->getRemoveRelation() && !$record->canUnlink(null, $gridField)) {
            return null;
        }

        return $field;
    }

    public function getGroup($gridField, $record, $columnName) {
        $field = parent::getGroup($gridField, $record, $columnName);

        if ($this->getRemoveRelation() && !$record->canUnlink(null, $gridField)) {
            return null;
        } else {
            return $field ? GridField_ActionMenuItem::DEFAULT_GROUP: null;
        }
    }
}
