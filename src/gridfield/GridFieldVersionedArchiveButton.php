<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Core\Convert;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridField_ColumnProvider;
use SilverStripe\Forms\GridField\GridField_ActionProvider;
use SilverStripe\Forms\GridField\GridField_ActionMenuItem;
use SilverStripe\Forms\GridField\GridField_FormAction;
use SilverStripe\Control\Controller;

class GridFieldVersionedArchiveButton implements GridField_ColumnProvider, GridField_ActionProvider, GridField_ActionMenuItem {

    protected $customActionName = 'ArchiveAction';
    protected $doCustomActionName = 'archive'; // lowercase only!

    public function getTitle($gridField, $record, $columnName) {
        return _t(self::class . '.Title', 'archive');
    }

    public function getCustomAction($gridField, $record) {
        if (!$record->canArchive()) {
            return;
        }

        return GridField_FormAction::create(
            $gridField,
            $this->customActionName . $record->ID,
            $this->customActionName,
            $this->doCustomActionName,
            ['RecordID' => $record->ID]
        )
            ->addExtraClass('action-menu--handled')
            ->setAttribute('classNames', 'font-icon-trash');
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if ($actionName !== $this->doCustomActionName) {
            return;
        }

        $record = $gridField->getList()->byID($arguments['RecordID']);
        if (!$record) {
            return;
        }

        $title = Convert::raw2xml($record->getTitle());

        if ($record->canArchive()) {
            $record->doArchive();
            $msg = _t(self::class . 'HasBeenArchived', '{title} has been archived', ['title' => $title]);
        } else {
            $msg = _t(self::class . 'CantBeArchived', '{title} could not be archived (insufficient permissions)', ['title' => $title]);
        }

        Controller::curr()->getResponse()->setStatusCode(
            200,
            $msg
        );
    }

    public function getColumnContent($gridField, $record, $columnName) {
        $field = $this->getCustomAction($gridField, $record);

        if (!$field) {
            return;
        }

        return $field->Field();
    }

    public function getExtraData($gridField, $record, $columnName) {
        $field = $this->getCustomAction($gridField, $record);

        if (!$field) {
            return;
        }

        return $field->getAttributes();
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return ['class' => 'grid-field__col-compact'];
    }

    public function getGroup($gridField, $record, $columnName) {
        $field = $this->getCustomAction($gridField, $record);

        return $field ? GridField_ActionMenuItem::DEFAULT_GROUP : null;
    }

    public function augmentColumns($gridField, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnMetadata($gridField, $columnName) {
        if ($columnName === 'Actions') {
            return ['title' => ''];
        }
    }

    public function getColumnsHandled($gridField) {
        return ['Actions'];
    }

    public function getActions($gridField) {
        return [$this->doCustomActionName];
    }
}

