<?php

namespace bhofstaetter\FrameworkTweaks;

use Closure;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridField_ActionMenu;
use SilverStripe\Forms\GridField\GridField_ActionMenuItem;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldButtonRow;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldDataColumns;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use SilverStripe\Forms\GridField\GridFieldFilterHeader;
use SilverStripe\Forms\GridField\GridFieldPageCount;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use SilverStripe\Forms\GridField\GridFieldViewButton;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\VersionedGridFieldItemRequest;
use SilverStripe\Versioned\VersionedGridFieldState\VersionedGridFieldState;
use Symbiote\GridFieldExtensions\GridFieldAddExistingSearchButton;
use Symbiote\GridFieldExtensions\GridFieldAddNewInlineButton;
use Symbiote\GridFieldExtensions\GridFieldAddNewMultiClass;
use Symbiote\GridFieldExtensions\GridFieldEditableColumns;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Unisolutions\GridField\CopyButton;

class GridFieldConfig_Extended extends GridFieldConfig {

    protected GridField $gridField;
    protected DataObject $modelSingleton;
    protected $modifiedGridFieldDataColumns;

    /**
     * <code>
     * $fields->addFieldsToTab('Root.Main', [
     *   $gf = GridField::create('RelationName', 'Relation Name', $this->RelationName()),
     * ]);
     *
     * 1. create config
     * $gc = GridFieldConfig_Extended::create($gf, 'SortOrder');
     *
     * 2. modify config
     * $gc->enableRelationEditor();
     *
     * 3. set config
     * $gf->setConfig($gc);
     * </code>
     */
    public function __construct(
        GridField $gridField,
        string $sortField = null,
        int $itemsPerPage = 50,
        bool $showPagination = null,
        bool $showAdd = null
    ) {
        parent::__construct();

        $this->gridField = $gridField;
        $this->modelSingleton = singleton($gridField->getModelClass());

        $filter = GridFieldFilterHeader::create();
        $filter->setThrowExceptionOnBadDataType(false);

        $paginator = GridFieldPaginator::create($itemsPerPage)
            ->setThrowExceptionOnBadDataType(false);

        $this->addComponents([
            GridFieldButtonRow::create('before'),
            GridFieldAddNewButton::create('buttons-before-left'),
            GridFieldToolbarHeader::create(),
            $this->createSortableHeaderComponent(),
            $filter,
            GridFieldDataColumns::create(),
            GridFieldEditButton::create(),
            GridFieldDeleteActionExtended::create(),
            GridField_ActionMenu::create(),
            GridFieldPageCount::create('toolbar-header-right'),
            $paginator,
            GridFieldDetailForm::create(null, $showPagination, $showAdd),
            new CopyButton()
        ]);

        if ($sortField) {
            $this->addComponent(GridFieldOrderableRows::create($sortField));
        }


        $this->extend('updateConfig');
    }

    public function createSortableHeaderComponent(): GridFieldSortableHeader {
        $sortableHeader = GridFieldSortableHeader::create()
            ->setThrowExceptionOnBadDataType(false);

        $fieldSorting = $this->modelSingleton->summaryFieldsSorting();

        if (count($fieldSorting)) {
            $sortableHeader->setFieldSorting(array_merge($sortableHeader->getFieldSorting(), $fieldSorting));
        }

        return $sortableHeader;
    }

    /**
     * <code>
     * $gc->modifyEditForm(function($record, $fields, $form, $itemRequest) {
     *   $fields->push(
     *     TextField::create('Foo', 'Foo')
     *       ->setValue($record->Foo)
     *   );
     * });
     * </code>
     */
    public function modifyEditForm(Closure $callback): void {
        $this->getComponentByType(GridFieldDetailForm::class)
            ->setItemEditFormCallback(function($form, $itemRequest) use ($callback) {
                call_user_func($callback, $form->getRecord(), $form->Fields(), $form, $itemRequest);
            });
    }

    /**
     * <code>
     * $gc->modifyDisplayFields(function($currentFields) {
     *   $currentFields['Foo'] = 'Foo';
     *   return $currentFields;
     * });
     * </code>
     */
    public function modifyDisplayFields(Closure $callback): void {
        $dataColumns = $this->getComponentByType(GridFieldDataColumns::class);
        $dataColumns->setDisplayFields(call_user_func(
            $callback,
            $dataColumns->getDisplayFields($this->gridField))
        );
        $this->modifiedGridFieldDataColumns = $dataColumns;
    }

    public function enableRelationEditor(): void {
        $this->addComponents([
            GridFieldAddExistingSearchButton::create('buttons-before-right'),
            GridFieldDeleteActionExtended::create(true)
        ]);
    }

    public function enableMultiClass(array $addableClasses = []): void {
        $sanitisedClasses = [];

        $this->removeComponentsByType(GridFieldAddNewButton::class);
        $addNewMultiClass = GridFieldAddNewMultiClass::create('buttons-before-left');

        $addableClasses = count($addableClasses)
            ? array_flip($addableClasses)
            : $addNewMultiClass->getClasses($this->gridField);

        foreach (array_keys($addableClasses) as $className) {
            $className = $this->unSanitiseClassName($className);
            $sanitisedClasses[$className] = singleton($className)->i18n_singular_name();
        }

        asort($sanitisedClasses);
        $addNewMultiClass->setClasses($sanitisedClasses);

        $this->addComponent($addNewMultiClass);

        $this->modifyDisplayFields(function ($currentFields) {
            $currentFields['singular_name'] = _t(__CLASS__ . '.Type', 'Type');
            return $currentFields;
        });
    }

    public function enableVersioning(array $labelFields = []): void {
        $this
            ->getComponentByType(GridFieldDetailForm::class)
            ->setItemRequestClass(VersionedGridFieldItemRequest::class);

        $versionedGridFieldState = new VersionedGridFieldState();

        if (count($labelFields)) {
            $versionedGridFieldState->setVersionedLabelFields($labelFields);
        }

        $this->addComponents([
            $versionedGridFieldState,
            new GridFieldVersionedPublishButton(),
            new GridFieldVersionedUnPublishButton(),
            new GridFieldVersionedArchiveButton(),
        ]);

        foreach ($this->getComponentsByType(GridFieldDeleteAction::class) as $action) {
            if (!$action->getRemoveRelation()) {
                $this->removeComponent($action);
            }
        }
    }

    /**
     * https://github.com/symbiote/silverstripe-gridfieldextensions/blob/3/docs/en/index.md#inline-editing
     *
     * <code>
     * $gc->enableInlineEditing(true, $inlineFields);
     * </code>
     */
    public function enableInlineEditing(bool $showEditButton = false, array $fields = []): void {
        $inlineFields = count($fields)
            ? $fields
            : $this->modelSingleton->gridFieldInlineFields();

        if (!$inlineFields) {
            return;
        }

        if ($showEditButton) {
            $this->gridField->addExtraClass('tweaks-open-edit-form-onclick');
        } else {
            $this->removeComponentsByType([
                GridFieldEditButton::class,
                GridFieldDetailForm::class,
            ]);
        }

        $this->removeComponentsByType([
            GridFieldAddNewButton::class,
            GridFieldDataColumns::class,
        ]);

        $editableColumns = GridFieldEditableColumns::create();
        $editableColumns->setDisplayFields($inlineFields);

        $this->addComponent($editableColumns, GridField_ActionMenuItem::class);
        $this->addComponent(GridFieldAddNewInlineButton::create());
    }

    /**
     * possible values inside the array:
     *
     * - "addExisting" to remove the possibility to add existing relations
     * - "addNew" to remove all kind of add new buttons
     * - "edit" to remove the edit form
     * - "search" to remove the search form
     * - "delete" to remove all kinds of delete actions
     * - "unlink" to remove the unlink action
     * - "view" to remove to possibility to view the record on separate page
     * - "sortableHeader" to remove the sortable header
     * - "copy" to remove the copy button
     */
    public function disable(array $args): void {
        $mapping = [
            'addExisting' => [
                GridFieldAddExistingAutocompleter::class,
                GridFieldAddExistingSearchButton::class
            ],
            'addNew' => [
                GridFieldAddNewButton::class,
                GridFieldAddNewInlineButton::class,
                GridFieldAddNewMultiClass::class,
            ],
            'edit' => [
                GridFieldEditButton::class,
                GridFieldAddNewInlineButton::class,
                GridFieldEditableColumns::class => GridFieldDataColumns::class,
            ],
            'search' => [
                GridFieldFilterHeader::class,
            ],
            'delete' => [
                GridFieldDeleteActionExtended::class,
                GridFieldVersionedArchiveButton::class,
            ],
            'unlink' => [
                GridFieldDeleteActionExtended::class,
            ],
            'view' => [
                GridFieldViewButton::class,
            ],
            'copy' => [
                CopyButton::class,
            ]
        ];

        foreach ($args as $arg) {
            if (isset($mapping[$arg])) {
                if ($arg === 'delete' || $arg === 'unlink') {
                    foreach ($mapping[$arg] as $component) {
                        if ($delActions = $this->getComponentsByType($component)) {
                            foreach ($delActions as $delAction) {
                                if ($arg === 'unlink') {
                                    $check = $delAction->getRemoveRelation();
                                } else {
                                    $check = !$delAction->getRemoveRelation();
                                }

                                if ($check) {
                                    $this->removeComponent($delAction);
                                }
                            }
                        }
                    }
                } else {
                    foreach ($mapping[$arg] as $oldComponent => $newComponent) {
                        if (is_string($oldComponent)) {
                            if ($newComponent == GridFieldDataColumns::class) {
                                $dataColumns = $this->modifiedGridFieldDataColumns;

                                if (!$dataColumns) {
                                    $dataColumns = GridFieldDataColumns::create();
                                }

                                $newComponent = $dataColumns;
                            } else {
                                $newComponent = $newComponent::create();
                            }

                            if (!$this->getComponentByType($newComponent::class)) {
                                $this->addComponent($newComponent, $oldComponent);
                            }

                            $this->removeComponentsByType($oldComponent);
                        } else {
                            $this->removeComponentsByType($newComponent);
                        }
                    }
                }
            }
        }

        if (!in_array('view', $args) && in_array('edit', $args)) {
            $this->addComponent(GridFieldViewButton::create());
        }
    }

    public function makeReadonly(): void {
        $this->addComponents([
            GridFieldViewButton::create(),
            GridFieldDetailForm::create(),
        ]);

        if (!$this->getComponentByType(GridFieldDataColumns::class)) {
            $dataColumns = $this->modifiedGridFieldDataColumns
                ? $this->modifiedGridFieldDataColumns
                : GridFieldDataColumns::create();

            $this->addComponent($dataColumns);
        }

        $this->removeComponentsByType([
            GridFieldAddNewButton::class,
            GridFieldEditButton::class,
            GridFieldDeleteAction::class,
            GridField_ActionMenu::class,
            GridFieldAddExistingAutocompleter::class,
            GridFieldAddExistingSearchButton::class,
            GridFieldEditableColumns::class,
            GridFieldAddNewInlineButton::class,
            GridFieldAddNewMultiClass::class,
            GridFieldOrderableRows::class,
            GridFieldVersionedArchiveButton::class,
            GridFieldVersionedPublishButton::class,
            GridFieldVersionedUnPublishButton::class,
        ]);
    }

    private function unSanitiseClassName(string $className): string {
        return str_replace('-', '\\', $className);
    }
}
