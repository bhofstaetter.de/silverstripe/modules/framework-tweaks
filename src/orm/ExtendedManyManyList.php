<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Control\Controller;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Core\Extension;
use SilverStripe\ORM\ManyManyList;
use SilverStripe\View\ArrayData;

class ExtendedManyManyList extends ManyManyList {

    public function add($item, $extraFields = null) {
        if (is_numeric($item)) {
            $addedObj = $this->dataClass::get()->byID($item);
        } else {
            if (in_array(Extension::class, ClassInfo::ancestry($item))) {
                $addedObj = $item->getOwner();
            } else {
                $addedObj = $item;
            }
        }

        $context = ArrayData::create([
           'item' => $addedObj,
           'extraFields' => $extraFields,
           'action' => Controller::curr()->getRequest()->param('Action'),
           'joinTable' => $this->getJoinTable(),
           'shouldBeAdded' => true,
           'list' => $this,
        ]);

        $this->extend('onBeforeAdd', $context);

        if ($addedObj && $addedObj->hasMethod('onBeforeAddToManyManyList')) {
            $addedObj->onBeforeAddToManyManyList($context);
        }

        $contextAsArray = $context->toMap();

        if (!$contextAsArray['shouldBeAdded']) {
            return;
        }

        parent::add($item, $contextAsArray['extraFields']);

        $this->extend('onAfterAdd', $context);

        if ($addedObj && $addedObj->hasMethod('onAfterAddToManyManyList')) {
            $addedObj->onAfterAddToManyManyList($context);
        }
    }

    public function removeByID($itemID) {
        $item = $this->dataClass::get()->byID($itemID);

        $context = ArrayData::create([
            'item' => $item,
            'extraFields' => $this->getExtraData(null, $itemID),
            'action' => Controller::curr()->getRequest()->param('Action'),
            'joinTable' => $this->getJoinTable(),
            'shouldBeRemoved' => true,
            'list' => $this,
        ]);

        $this->extend('onBeforeRemoveByID', $context);

        if ($item && $item->hasMethod('onBeforeRemoveFromManyManyList')) {
            $item->onBeforeRemoveFromManyManyList($context);
        }

        if (!$context->toMap()['shouldBeRemoved']) {
            return;
        }

        parent::removeByID($itemID);

        $this->extend('onAfterRemoveByID', $context);

        if (is_object($item) && $item->hasMethod('onAfterRemoveFromManyManyList')) {
            $item->onAfterRemoveFromManyManyList($context);
        }
    }
}
