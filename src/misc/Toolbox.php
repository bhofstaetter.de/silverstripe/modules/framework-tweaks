<?php

namespace bhofstaetter\FrameworkTweaks;

use Psr\Log\LoggerInterface;
use SilverStripe\Admin\LeftAndMain;
use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\DevBuildController;
use SilverStripe\Dev\DevelopmentAdmin;
use SilverStripe\ORM\DatabaseAdmin;
use SilverStripe\ORM\FieldType\DBHTMLVarchar;
use SilverStripe\View\TemplateGlobalProvider;

class Toolbox implements TemplateGlobalProvider {

    public static function is_backend(): bool {
        if (!Controller::has_curr()) {
            return false;
        }

        $ctrl = Controller::curr();

        if (
            $ctrl instanceof LeftAndMain
            || is_subclass_of($ctrl, LeftAndMain::class)
            || self::on_dev_build()
        ) {
            return true;
        }

        return false;
    }

    public static function on_dev_build(): bool {
        if (!Controller::has_curr()) {
            return false;
        }

        $ctrl = Controller::curr();

        if (
            $ctrl instanceof DevelopmentAdmin
            || $ctrl instanceof DatabaseAdmin
            || $ctrl instanceof DevBuildController
            || is_subclass_of($ctrl,DevelopmentAdmin::class)
            || is_subclass_of($ctrl,DatabaseAdmin::class)
            || is_subclass_of($ctrl, DevBuildController::class)
        ) {
            return true;
        }

        return false;
    }

    public static function is_frontend(): bool {
        return !self::is_backend();
    }

    public static function generate_token(
        int $length = 32,
        string $classNameToCheck = null,
        string $fieldToCheck = null
    ): string {
        if ($classNameToCheck && $fieldToCheck) {
            do {
                $token = bin2hex(random_bytes($length/2));
            } while ($classNameToCheck::get()->find($fieldToCheck, $token));
        } else {
            $token = bin2hex(random_bytes($length/2));
        }

        return $token;
    }

    public static function get_session(): SilverStripe\Control\Session {
        return Injector::inst()
            ->get(HTTPRequest::class)
            ->getSession();
    }

    public static function get_logger(): LoggerInterface {
        return Injector::inst()
            ->get(LoggerInterface::class);
    }

    public static function url_add_scheme($url, $scheme = 'https://'): ?string {
        return $url
            ? (parse_url($url, PHP_URL_SCHEME) === null ? $scheme . $url : $url)
            : null;
    }

    public static function formatted_string(string $string, array $pattern = null): DBHTMLVarchar {
        if (!$pattern) {
            $pattern = [
                '<' => '<span>',
                '>' => '</span>'
            ];
        }

        return DBHTMLVarchar::create()
            ->setValue(nl2br(strtr($string, $pattern)));
    }

    public static function clean_phone_number($phoneNumber): string {
        $phoneNumber = trim($phoneNumber);

        if (str_starts_with($phoneNumber, '+')) {
            $phoneNumber = '00' . $phoneNumber . str_replace('(0)', '', $phoneNumber);
        }

        return preg_replace('/[^0-9]/', '', $phoneNumber);
    }

    public static function get_template_global_variables(): array {
        return [
            'CleanPhoneNumber' => 'clean_phone_number',
            'UrlAddScheme' => 'url_add_scheme',
        ];
    }
}

