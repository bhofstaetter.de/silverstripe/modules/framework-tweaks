<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Forms\FieldGroup;
use SilverStripe\Forms\FormField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\ORM\FieldType\DBHTMLText;

class NoticeField extends LiteralField {

    public const ERROR = 'error';
    public const GOOD = 'good';
    public const NEUTRAL = 'neutral';
    public const NOTICE = 'notice';
    public const WARNING = 'warning';

    protected bool $hidden;
    protected string $severity;
    protected ?string $fieldGroupTitle;

    protected $schemaDataType = FormField::SCHEMA_DATA_TYPE_STRUCTURAL;
    protected $schemaComponent = 'LiteralField';

    public function __construct(
        $name,
        $content,
        $hidden = false,
        $severity = self::NOTICE,
        $fieldGroupTitle = null,
    ) {
        $this->setSeverity($severity);
        $this->setHidden($hidden);
        $this->setFieldGroupTitle($fieldGroupTitle);

        parent::__construct($name, $content);
    }

    public function Field($properties = []): DBHTMLText
    {
        return $this->renderWith($this->getTemplates());
    }

    public function FieldHolder($properties = []): DBHTMLText
    {
        return $this->Field();
    }

    public function SmallFieldHolder($properties = []): DBHTMLText
    {
        return $this->Field();
    }

    /**
     * @return bool
     */
    public function getHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return NoticeField
     */
    public function setHidden(bool $hidden): NoticeField
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeverity(): string
    {
        return $this->severity;
    }

    /**
     * @param string $severity
     * @return NoticeField
     */
    public function setSeverity(string $severity): NoticeField
    {
        $this->severity = $severity;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldGroupTitle(): ?string
    {
        return $this->fieldGroupTitle;
    }

    /**
     * @param string|null $fieldGroupTitle
     * @return NoticeField
     */
    public function setFieldGroupTitle(?string $fieldGroupTitle): NoticeField
    {
        $this->fieldGroupTitle = $fieldGroupTitle;
        return $this;
    }
}
