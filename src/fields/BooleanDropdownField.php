<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Forms\DropdownField;

class BooleanDropdownField {

    /**
     * A dropdown field for boolean values
     * TODO: getter & setter
     *
     * @param string $name Database field
     * @param string|null $title Field label.
     * @param bool $val Selected default value, true or false, will not work on versioned classes
     * @param string|null $trueLabel Label for true
     * @param string|null $falseLabel Label for false
     */
    public static function create(
        string $name,
        string|null $title = null,
        bool $val = false,
        string|null $trueLabel = null,
        string|null $falseLabel = null,
    ) {
        return DropdownField::create(
            $name,
            $title,
            [
                true => $trueLabel ?: _t(__CLASS__ . '.TrueValue', 'Yes'),
                false => $falseLabel ?: _t(__CLASS__ . '.FalseValue', 'No'),
            ],
            $val
        );
    }
}
