<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Core\Extension;
use SilverStripe\Forms\Form;

class GridFieldValidationExemptExtension extends Extension {

    public function updateItemEditForm(Form $form): void {
        $form->setValidationExemptActions([
            'restore',
            'revert',
            'deletefromlive',
            'delete',
            'unpublish',
            'rollback',
            'doRollback',
            'archive',
            'doDelete',
        ]);
    }
}
