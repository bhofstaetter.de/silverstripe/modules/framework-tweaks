<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Control\Controller;
use SilverStripe\Forms\CompositeValidator;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\Permission;
use SilverStripe\Versioned\Versioned;
use SilverStripe\View\ArrayData;

class DataObjectExtension extends DataExtension {

    /**
     * always execute onAfterWrite
     */
    private static bool $force_on_after_write = false;

    /**
     * max allowed records from the given class
     */
    private static int $max_items_count = 0;

    /**
     * array(s) with a single db field name or multiple db field names.
     * inside one array, the fields will be checked with a && operator
     * multiple arrays will be checked with a || operator
     *
     * <code>
     * [
     *   ['Title', 'ParentID'],
     *   ['RelationID'],
     * ]
     * </code>
     *
     * That means, in this example no entries allowed either with the same Title AND ParentID or the same RelationID
     */
    private static array $duplicate_check_conditions = [];


    /**
     * 'Unsortable summary entry' => 'Sortable database field'
     */
    private static array $summary_fields_sorting = [
        'Created.Nice' => 'Created',
        'LastEdited.Nice' => 'LastEdited',
        'Active.Nice' => 'Active',
        'Enabled.Nice' => 'Enabled',
        'Show.Nice' => 'Show',
        'singular_name' => 'ClassName',
        'plural_name' => 'ClassName',
    ];

    /**
     * https://github.com/symbiote/silverstripe-gridfieldextensions/blob/3/docs/en/index.md#inline-editing
     */
    private static array $grid_field_inline_fields = [];

    private static array $required_fields = [];

    private static string $basic_fieldset_name_or_title = 'Name';


    // Can Methods -----------------------------------------------------------------------------------------------------

    public function canCreate($member = null, $context = []): bool {
        $result = ValidationResult::create();
        $this->validateMaxItemsCount($result);
        return $result->isValid();
    }

    public function canUnlink($member = null): bool {
        $canUnlink = Permission::check('ADMIN', 'any', $member);

        if ($this->owner->hasMethod('updateCanUnlink')) {
            $this->owner->updateCanUnlink($canUnlink);
        }

        $this->owner->extend('updateCanUnlink', $canUnlink);

        return $canUnlink;
    }

    // Public API ------------------------------------------------------------------------------------------------------

    public function forceOnAfterWrite(): bool {
        $force = $this->owner->config()->get('force_on_after_write');

        if ($this->owner->hasMethod('updateForceOnAfterWrite')) {
            $this->owner->updateForceOnAfterWrite($force);
        }

        $this->owner->extend('updateForceOnAfterWrite', $force);

        return $force;
    }

    public function maxItemsCount(): int {
        $count = $this->owner->config()->get('max_items_count');

        if ($this->owner->hasMethod('updateMaxItemsCount')) {
            $this->owner->updateMaxItemsCount($count);
        }

        $this->owner->extend('updateMaxItemsCount', $count);

        return $count;
    }

    public function duplicateCheckConditions(): array {
        $conditions = $this->owner->config()->get('duplicate_check_conditions');

        if ($this->owner->hasMethod('updateDuplicateCheckConditions')) {
            $this->owner->updateDuplicateCheckConditions($conditions);
        }

        $this->owner->extend('updateDuplicateCheckConditions', $conditions);

        return $conditions;
    }

    public function summaryFieldsSorting(): array {
        $fieldsSorting = $this->owner->config()->get('summary_fields_sorting');

        if ($this->owner->hasMethod('updateSummaryFieldsSorting')) {
            $this->owner->updateSummaryFieldsSorting($fieldsSorting);
        }

        $this->owner->extend('updateSummaryFieldsSorting', $fieldsSorting);

        return $fieldsSorting;
    }

    public function gridFieldInlineFields(): array {
        $inlineFields = $this->owner->config()->get('grid_field_inline_fields');

        if ($this->owner->hasMethod('updateGridFieldInlineFields')) {
            $this->owner->updateGridFieldInlineFields($inlineFields);
        }

        $this->owner->extend('updateGridFieldInlineFields', $inlineFields);

        return $inlineFields;
    }

    public function requiredFields(): array {
        $requiredFields = $this->owner->config()->get('required_fields');

        if ($this->owner->hasMethod('updateRequiredFields')) {
            $this->owner->updateRequiredFields($requiredFields);
        }

        $this->owner->extend('updateRequiredFields', $requiredFields);

        return $requiredFields;
    }

    public function onBeforeAddToManyManyList(ArrayData $context): void {
        if ($this->owner->hasMethod('updateOnBeforeAddToManyManyList')) {
            $this->owner->updateOnBeforeAddToManyManyList($context);
        }

        $this->owner->extend('updateOnBeforeAddToManyManyList', $context);
    }

    public function onAfterAddToManyManyList(ArrayData $context): void {
        if ($this->owner->hasMethod('updateOnAfterAddToManyManyList')) {
            $this->owner->updateOnAfterAddToManyManyList($context);
        }

        $this->owner->extend('updateOnAfterAddToManyManyList', $context);
    }

    public function onBeforeRemoveFromManyManyList(ArrayData $context): void {
        if ($this->owner->hasMethod('updateOnBeforeRemoveFromManyManyList')) {
            $this->owner->updateOnBeforeRemoveFromManyManyList($context);
        }

        $this->owner->extend('updateOnBeforeRemoveFromManyManyList', $context);
    }

    public function onAfterRemoveFromManyManyList(ArrayData $context): void {
        if ($this->owner->hasMethod('updateOnAfterRemoveFromManyManyList')) {
            $this->owner->updateOnAfterRemoveFromManyManyList($context);
        }

        $this->owner->extend('updateOnAfterRemoveFromManyManyList', $context);
    }

    public function basicFieldsetNameOrTitle(): string {
        $nameOrTitle = $this->owner->config()->get('basic_fieldset_name_or_title');

        if ($this->owner->hasMethod('updateBasicFieldSetNameOrTitle')) {
            $this->owner->updateBasicFieldSetNameOrTitle($nameOrTitle);
        }

        $this->owner->extend('updateBasicFieldSetNameOrTitle', $nameOrTitle);

        return $nameOrTitle;
    }

    public function getBasicCMSFields($showDescription = false, $withSettingsTab = false) {
        $fields = FieldList::create(
            TabSet::create('Root',
                Tab::create('Main', _t(__CLASS__ . '\FieldLabels.Main', 'Main')),
                Tab::create('Settings',  _t(__CLASS__ . '\FieldLabels.Settings', 'Settings'))
            )
        );

        if ($withSettingsTab) {
            $tabName = 'Settings';
        } else {
            $tabName = 'Main';
            $fields->removeByName('Settings');
        }

        $dbFields = $this->owner->config()->get('db');

        if (array_key_exists('Title', $dbFields) && array_key_exists('Name', $dbFields)) {
            $titleField = $this->basicFieldsetNameOrTitle();
        } else if (array_key_exists('Title', $dbFields)) {
            $titleField = 'Title';
        } else if (array_key_exists('Name', $dbFields)) {
            $titleField = 'Name';
        }

        if (isset($titleField)) {
            $fields->addFieldToTab('Root.Main', TextField::create($titleField,  _t(__CLASS__ . '\FieldLabels.' . $titleField, 'Name')));
        }

        if ($activeField = $this->getBooleanOrEnumDropdownField('Active', $showDescription)) {
            $fields->addFieldToTab('Root.' . $tabName, $activeField);
        }

        if ($showField = $this->getBooleanOrEnumDropdownField('Show', $showDescription)) {
            $fields->addFieldToTab('Root.' . $tabName, $showField);
        }

        $this->owner->extend('updateBasicCMSFields', $fields, $showDescription, $withSettingsTab);

        return $fields;
    }

    public function ClassNameForTemplate(): string {
        $parts = explode('\\', $this->owner->ClassName);

        if (count($parts) >= 3) {
            $className = $parts[0] . '_' . $parts[1] . '_' . $parts[count($parts) - 1];
        } else {
            $className = implode('_', $parts);
        }

        return strtolower($className);
    }

    // Private Processing ----------------------------------------------------------------------------------------------

    public function onBeforeWrite() {
        if ($this->forceOnAfterWrite()) {
            $this->owner->LastEdited = time();
        }
    }

    public function updateCMSCompositeValidator(CompositeValidator $compositeValidator): void {
        $compositeValidator->addValidator(RequiredFields::create($this->requiredFields()));
    }

    public function validate(ValidationResult $result): void {
        if (!$this->skipSpecificValidation('skipMaxItemsCountValidation')) {
            $this->validateMaxItemsCount($result);
        }

        if (!$this->skipSpecificValidation('skipDuplicateCheckConditionsValidation')) {
            $this->validateDuplicateCheckConditions($result);
        }

        if (!$this->skipSpecificValidation('skipRequiredFieldsValidation')) {
            $this->validateRequiredFields($result);
        }
    }

    private function skipSpecificValidation($methodName): bool
    {
        $skip = false;

        if ($this->owner->hasMethod($methodName)) {
            $this->owner->$methodName($skip);
        }

        $this->owner->extend($methodName, $skip);

        return $skip;
    }

    private function validateRequiredFields(ValidationResult $result): void {
        foreach ($this->requiredFields() as $k => $v) {
            if (is_string($k)) {
                $requiredField = $k;
                $customErrorMessage = $v;
            } else {
                $requiredField = $v;
                $customErrorMessage = false;
            }

            $dbField = $this->owner->dbObject($requiredField);

            if (!$dbField) {
                $computedValue = true;

                // todo: respect versioning?

                if (isset($this->owner->hasMany()[$requiredField])) {
                    $computedValue = $this->owner->$requiredField()->count();
                } else if (isset($this->owner->manyMany()[$requiredField])) {
                    $computedValue = $this->owner->$requiredField()->count();
                } else if ($this->owner->hasMethod($requiredField)) {
                    $computedValue = $this->owner->$requiredField();
                }
            }

            if (
                ($dbField && ($dbField->getValue() === null || $dbField->getValue() === ''))
                || (!$dbField && !$computedValue)
            ) {
                $result->addFieldError(
                    $requiredField,
                    is_string($customErrorMessage)
                        ? $customErrorMessage
                        : _t(Form::class . '.FIELDISREQUIRED', $requiredField . ' is required', ['name' => '"' . $requiredField . '"'])
                );
            }
        }
    }

    private function validateMaxItemsCount(ValidationResult $result): void {
        $maxCount = $this->maxItemsCount();

        if ($maxCount <= 0) {
            return;
        }

        $currentCount = $this->owner->ClassName::get()
            ->exclude('ID', $this->owner->ID)
            ->count();

        if ($maxCount >= $currentCount) {
            return;
        }

        $result->addError(
            _t('bhofstaetter\\FrameworkTweaks\\DataObjectExtension.MaxItemsError', 'NoTranslation {count}|MaxItemsError {count}', [
                'count' => $maxCount,
                'singularName' => $this->owner->i18n_singular_name(),
                'pluralName' => $this->owner->i18n_plural_name(),
            ]),
        );
    }

    private function validateDuplicateCheckConditions(ValidationResult $result): void {
        $rawConditions = $this->duplicateCheckConditions();
        $conditions = [];

        foreach ($rawConditions as $fields) {
            $filteredFields = array_filter($fields);

            if (count($filteredFields)) {
                $conditions[] = $filteredFields;
            }
        }

        if (!count($conditions)) {
            return;
        }

        $filters = [];

        foreach ($conditions as $key => $fields) {
            $ignoreEmptyKey = array_search('IGNORE_EMPTY', $fields);

            if ($ignoreEmptyKey !== false) {
                unset($fields[$ignoreEmptyKey]);
                $newFilters = [];

                foreach ($fields as $field) {
                    $fieldValue = $this->owner->$field;

                    if ($fieldValue) {
                        $newFilters[$field] = $fieldValue;
                    }

                    if (count($newFilters) > 1) {
                        $filters[$key] = $newFilters;
                    }
                }
            } else {
                foreach ($fields as $field) {
                    $filters[$key][$field] = $this->owner->$field;
                }
            }
        }

        $currentReadingMode = Versioned::get_reading_mode();

        if ($this->owner->hasExtension(Versioned::class)) {
            Versioned::set_reading_mode('Stage.' . Versioned::LIVE);
        }

        $items = $this->owner->ClassName::get()
            ->exclude('ID', $this->owner->ID);

        foreach ($filters as $filter) {
            if ($item = $items->filter($filter)->first()) {
                $result->addError(
                    _t('bhofstaetter\\FrameworkTweaks\\DataObjectExtension.DuplicateCheckError', 'NoTranslation {count}|DuplicateCheckError {count}', [
                        'count' => count($filter),
                        'singularName' => $this->owner->i18n_singular_name(),
                        'title' => $item->Title,
                        'id' => $item->ID,
                        'fields' => implode(', ', array_keys($filter)),
                    ]),
                );

                break;
            }
        }

        Versioned::set_reading_mode($currentReadingMode);
    }

    private function getBooleanOrEnumDropdownField($fieldName, $showDescription): DropdownField|BooleanDropdownField|null {
        $dbFields = $this->owner->config()->get('db');

        if (array_key_exists($fieldName, $dbFields)) {
            $dbFieldType = $dbFields[$fieldName];
            $fieldLabel = _t(__CLASS__ . '\FieldLabels.' . $fieldName, $fieldName);

            if (str_starts_with($dbFieldType, 'Boolean')) {
                $field = BooleanDropdownField::create($fieldName, $fieldLabel, preg_replace('/[^0-9]/', '', $dbFieldType));
            } else if (str_starts_with($dbFieldType, 'Enum')) {
                $field = DropdownField::create($fieldName, $fieldLabel, $this->owner->dbObject($fieldName)->niceEnumValues());
            }

            if (isset($field)) {
                $desc = _t(__CLASS__ . '\FieldDescription.' . $fieldName, ' ', ['type' => $this->owner->singular_name()]);

                if (trim($desc) && $showDescription) {
                    $field->setDescription($desc);
                }

                return $field;
            }
        }

        return null;
    }

    public function onBeforeDuplicate($original, $doWrite, $relations): void {
        $this->preventDuplicateCheckErrorOnDuplication($original, $doWrite, $relations);
    }

    private function preventDuplicateCheckErrorOnDuplication($original, $doWrite, $relations): void {
        $conditions = $this->duplicateCheckConditions();

        if (!count($conditions)) {
            return;
        }

        foreach ($conditions as $fields) {
            foreach ($fields as $field) {
                if (!str_ends_with($field, 'ID')) {
                    continue;
                }

                $relationName = substr($field, 0, strlen($field) - 2);

                if (!$relationName|| !$this->owner->hasMethod($relationName)) {
                    $this->owner->$field = $this->owner->$field . ' [' . _t('bhofstaetter\\FrameworkTweaks.Copy', 'Copy') . ' ' . time() . ']';
                }
            }
        }
    }

    public function updateFieldLabels(&$labels): void {
        $ctrl = Controller::curr();
        $cache = $ctrl->_bftFieldLabelsCache;

        asort($labels);

        $trace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 4)[3];
        $ident = implode('__', [
            $this->owner->ClassName,
            stripslashes($trace['class']),
            $trace['function'],
            implode('_', array_keys($labels)),
        ]);

        if (isset($cache[$ident])) {
            $labels = $cache[$ident];
        } else {
            // prevent infinity-loops
            $cache[$ident] = $labels;
            $ctrl->_bftFieldLabelsCache = $cache;

            $fields = $this->owner->getCMSFields()->flattenFields();
            $moreFieldLabels = [
                'Created.Nice' => _t(__CLASS__ . '\FieldLabels.Created', 'created'),
                'Created' => _t(__CLASS__ . '\FieldLabels.Created', 'created'),
                'LastEdited.Nice' => _t(__CLASS__ . '\FieldLabels.LastEdited', 'last edited'),
                'LastEdited' => _t(__CLASS__ . '\FieldLabels.LastEdited', 'last edited'),
            ];

            foreach ($labels as $name => &$title) {
                $field = $fields->fieldByName($name);

                if ($field && $field->hasMethod('Title') && $field->Title()) {
                    $title = $field->Title();
                    $moreFieldLabels[$name . '.Nice'] = $title;
                    $moreFieldLabels[$name . '.Count'] = $title;
                }
            }

            $labels = array_merge($moreFieldLabels, $labels);

            $cache[$ident] = $labels;
            $ctrl->_bftFieldLabelsCache = $cache;
        }
    }
}
