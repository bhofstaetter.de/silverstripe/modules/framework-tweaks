<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Core\Extension;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldExportButton;
use SilverStripe\Forms\GridField\GridFieldFilterHeader;
use SilverStripe\Forms\GridField\GridFieldImportButton;
use SilverStripe\Forms\GridField\GridFieldPrintButton;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use SilverStripe\Versioned\Versioned;

class ModelAdminExtension extends Extension {

    public $showImportForm = false;
    public $showSearchForm = true;

    public function updateGridField(GridField $gridField) {
        $config = GridFieldConfig_Extended::create($gridField);

        $exportButton = Injector::inst()->createWithArgs(GridFieldExportButton::class, ['buttons-before-left']);
        $exportButton->setExportColumns($this->getExportFields());

        $config
            ->addComponent($exportButton)
            ->addComponents(Injector::inst()->createWithArgs(GridFieldPrintButton::class, ['buttons-before-left']));

        if (!$this->showSearchForm ||
            (is_array($this->showSearchForm) && !in_array($this->modelClass, $this->showSearchForm ?? []))
        ) {
            $config->removeComponentsByType(GridFieldFilterHeader::class);
        }

        if ($this->showImportForm) {
            $config->addComponent(
                GridFieldImportButton::create('buttons-before-left')
                    ->setImportForm($this->owner->ImportForm())
                    ->setModalTitle(_t(ModelAdmin::class . '.IMPORT', 'Import from CSV'))
            );
        }

        if ($modelClass = $this->unsanitiseClassName($this->owner->getRequest()->param('ModelClass'))) {
            if (singleton($modelClass)->hasExtension(Versioned::class)) {
                $config->enableVersioning();
            }
        }

        $gridField->setConfig($config);
    }

    public function getExportFields() {
        if ($modelClass = $this->unsanitiseClassName($this->owner->getRequest()->param('ModelClass'))) {
            $model = singleton($modelClass);

            if ($model->hasMethod('getExportFields')) {
                return $model->getExportFields();
            } else {
                return $model->SummaryFields();
            }
        }
    }

    protected function unsanitiseClassName($class)
    {
        return str_replace('-', '\\', $class ?? '');
    }
}
