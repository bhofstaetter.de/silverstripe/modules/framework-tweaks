<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Core\Extension;

class DBEnumExtension extends Extension {

    public function Nice(): string {
        return _t(__CLASS__ . '\NiceEnumValues.' . $this->owner->Plain(), $this->owner->Plain());
    }

    public function niceEnumValues($hasEmpty = false): array {
        $values = $this->getOwner()->enumValues();

        array_walk($values, function (&$v, $k) {
            $v = _t(__CLASS__ . '\NiceEnumValues.' . $k, $v);
        });

        return $values;
    }
}
