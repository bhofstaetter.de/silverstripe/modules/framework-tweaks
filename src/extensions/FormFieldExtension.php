<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Core\Extension;
use SilverStripe\Forms\FormField;

class FormFieldExtension extends Extension {

    public function onBeforeRenderHolder(FormField $context, array &$properties) {
        if ($context->Required()) {
            $context->addExtraClass('requiredField');
            $properties['RequiredField'] = true;
        }
    }
}
