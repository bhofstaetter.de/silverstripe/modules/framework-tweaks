<?php

namespace bhofstaetter\FrameworkTweaks;

use SilverStripe\Admin\CMSMenu;
use SilverStripe\Core\Extension;
use SilverStripe\View\Requirements;
use SilverStripe\View\SSViewer;

class LeftAndMainExtension extends Extension
{
    private static $allowed_actions = [
        'clearCache',
    ];

    public function init() {
        $this->removeMenuItems();
        $this->loadCustomRequirements();
    }

    private function removeMenuItems() {
        if ($menusToHide = $this->owner->config()->get('remove_menu_items')) {
            foreach ($menusToHide as $menuName) {
                CMSMenu::remove_menu_item($menuName);
            }
        }
    }

    private function loadCustomRequirements() {
        if ($resources = $this->owner->config()->get('custom_requirements_css')) {
            foreach($resources as $resource) {
                Requirements::css($resource);
            }
        }

        if ($resources = $this->owner->config()->get('custom_requirements_javascript')) {
            foreach($resources as $resource) {
                Requirements::javascript($resource);
            }
        }
    }

    public function clearCache() {
        SSViewer::flush();

        return
            '<p style="
                font-family: sans-serif;
                text-align: center;
                margin-top: 30px;
            ">
                <strong>Der Silverstripe Cache wurde geleert.</strong><br>Sie können diesen Tab nun wieder schließen [STRG] + [W].
            </p>';
    }
}

