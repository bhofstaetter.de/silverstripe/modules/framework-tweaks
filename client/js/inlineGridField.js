(function($) {
  $('.tweaks-open-edit-form-onclick .ss-gridfield-items tr td .readonly').entwine({
    onmatch: function () {
      let col = $(this).closest('td');
      let editLink = col.closest('tr').find('a.edit-link');

      if (editLink.length) {
        col.click(function (e) {
          window.location = editLink.attr('href');
        });
      }
    }
  });
}(jQuery));
