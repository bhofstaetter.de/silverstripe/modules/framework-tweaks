# Silverstripe Framework Tweaks

This module includes several, opinionated improvements to work with the silverstripe framework and admin

## Installation

```
composer require bhofstaetter/framework-tweaks
```

### Setup CronTask cronjob

```
* * * * * www-data /usr/bin/php /path/to/silverstripe/docroot/vendor/bin/sake dev/cron
```

### Fields

- ``BooleanDropdownField::create($name, $title, $defaultVal, $trueLabel, $falseLabel)``
- ``NoticeField::create($name, $content, $hidden, $severity, $fieldGroupTitle)``

### UI

- Fontawesome 6 icons for menu and SiteTree. To use FA Pro switch out ``SilverStripe\Admin\LeftAndMain.custom_requirements_css.fontawesome``
- CMS preview defaults to "Content"
- Remove toast messages with a click on the body
- Adds help link to clear cache in the admin area
- Remove unused/unwanted menu items through ``SilverStripe\Admin\LeftAndMain.remove_menu_items``

### ExtendedManyManyList

Is a replacement for the ManyManyList class that brings some cool new features

#### Inside an object or data extension

- ``updateOnBeforeAddToManyManyList($context)``
- ``updateOnAfterAddToManyManyList($context)``
- ``updateOnBeforeRemoveFromManyManyList($context)``
- ``updateOnAfterRemoveFromManyManyList($context)``

#### Inside manyManyList extensions

- ``onBeforeAdd($context)``
- ``onAfterAdd($context)``
- ``onBeforeRemoveByID($context)``
- ``onAfterRemoveByID($context)``

#### $context Object

Is an ArrayData object with the following values

- ``item``
- ``extraFields``
- ``action``
- ``joinTable``
- ``shouldBeAdded/shouldBeRemoved``
- ``list``

### DataObject

- ``$force_on_after_write`` or ``forceOnAfterWrite(&$force)``
- ``$max_items_count`` or ``updateMaxItemsCount(&$count)``
- ``$duplicate_check_conditions`` or ``updateDuplicateCheckConditions(&$conditions)``
- ``$summary_fields_sorting`` or ``updateSummaryFieldsSorting(&$fields)``
- ``$grid_field_inline_fields`` or ``updateGridFieldInlineFields(&$fields)``
- ``$requiredFields`` or ``updateRequiredFields(&$fields)``
- ``updateCanUnlink($member = null)`` DO NOT USE ``canUnlink`` directly!
- ``ClassNameForTemplate()``
- ``getBasicCMSFields($showDescription = false, $removeEmptySettingsTab = true)``

#### Required Fields

Could be a database field name (truthy value => valid), a relation name of a has_many or many_many relation (count > 0 => valid)
or a callable method name of this class (truthy value => valid).

````php
private static $required_fields = [
    'Name',
    'Pages' => 'Add at least one Page',
    'MethodOnClass' => 'Custom Error Message',
];
````

### GridFieldConfig_Extended

```php
$fields->addFieldsToTab('Root.Main', [
  $gf = GridField::create('RelationName', 'Relation Name', $this->RelationName()),
]);

$gc = GridFieldConfig_Extended::create($gf, 'SortOrder');
$gc->enableRelationEditor();
$gf->setConfig($gc);
```

- ``modifyEditForm(function($record, $fields, $form, $itemRequest) {...})``
- ``modifyDisplayFields(function($currentFields) {...})``
- ``enableRelationEditor()``
- ``enableMultiClass()``
- ``enableVersioning()``
- ``enableInlineEditing()``
- ``disable()``
- ``makeReadonly()``

### Toolbox Class

- ``Toolbox::is_backend()``
- ``Toolbox::on_dev_build()``
- ``Toolbox::is_frontend()``
- ``Toolbox::generate_token(int $length = 32, string $classNameToCheck = null, string $fieldToCheck = null)``
- ``Toolbox::get_session()``
- ``Toolbox::get_logger()``
- ``Toolbox::url_add_scheme($url, $scheme = 'https://')`` or ``$UrlAddScheme($url, $scheme = 'https://')``
- ``Toolbox::formatted_string(string $string, array $pattern = null)``
- ``Toolbox::clean_phone_number($phoneNumber)`` or ``$CleanPhoneNumber($phoneNumber)``

### Nice Enum Values

all values

```php
$this->dbObject('EnumField')->niceEnumValues();
```

selected value

```php
private static $summary_fields = [
	'EnumField.Nice' => 'Enum',
];
```

put your enum translations beneath ``bhofstaetter\FrameworkTweaks\DBEnumExtension\NiceEnumValues``

### Misc

- Adds and changes a few german translations
- Add labels from CMS Fields to SummaryFields, Search, .. through updateFieldLabels
- Prevents validation on delete, restore, ... on DataObjects
- Use ``GridFieldConfig_Extended`` for ModelAdmins
- When exporting data through ModelAdmins, if present on the DataObject, the results from ``getExportFields()`` will be used
- Sets ``SilverStripe\View\SSViewer.rewrite_hash_links`` to ``false``
- Sets ``SilverStripe\View\SSViewer.global_key`` to ``$CurrentReadingMode, $CurrentUser.ID, $CurrentLocale, $BaseHref``

## Included Modules

- [GridField Extensions](https://github.com/symbiote/silverstripe-gridfieldextensions)
- [Sortable UploadField](https://github.com/bummzack/sortablefile)
- [Display Logic](https://github.com/unclecheese/silverstripe-display-logic)
- [CMS Actions](https://github.com/lekoala/silverstripe-cms-actions)
- [Copy Button](https://github.com/restruct/silverstripe-copybutton)
- [CronTask](https://github.com/silverstripe/silverstripe-crontask)
- [Trailing Slash](https://github.com/axllent/silverstripe-trailing-slash)

## Todo

- ExtendedHasManyList
- wrapper-field for two/three/four cols per row
- wrapper-field for two cols layout
- faFileIcon
- inherit page permissions for DataObjects
- improve ExtendedManyManyList code
- improve + clean up custom GridField buttons
